//
// Created by user on 22.11.17.
//

#include <iostream>
#include "Test.h"
#include "Block.h"
#include "SBox.h"

void Test::invariants() {
    auto blockBytes = new byte[16] {
             11, 22, 33, 44,
             55, 66, 77, 88,
             99, 111, 122, 133,
             144, 155, 166, 177
    };

    auto block = new Block(blockBytes);
    block->printAsMatrix();

    cout << "Test gamma" << endl;
    block->gamma();
    block->printAsMatrix();
    block->gamma();
    cout << "Should be equal to origin:" << endl;
    block->printAsMatrix();

    cout << "Test tau" << endl;
    block->tau();
    block->printAsMatrix();
    block->tau();
    cout << "Should be equal to origin:" << endl;
    block->printAsMatrix();

    cout << "Test theta" << endl;
    block->theta();
    block->printAsMatrix();
    block->theta();
    block->printAsMatrix();

    cout << "Test sigma" << endl;
    block->sigma(block);
    cout << "Should be zero matrix:" << endl;
    block->printAsMatrix();
}

void Test::sbox() {
    for (int i = 0; i < 0xff; i++) {
        byte s = SBox::bytes[i];
        byte ss = SBox::bytes[s];
        if (i != ss) {
            throw 1;
        }
    }
}

void Test::invariantsSequence() {
    auto blockBytes = new byte[16] {
            11, 22, 33, 44,
            55, 66, 77, 88,
            99, 111, 122, 133,
            144, 155, 166, 177
    };

    auto block = new Block(blockBytes);
    block->printAsMatrix();

    auto roundKeyBytes = new byte[16] {
            4, 8, 16, 32,
            8, 32, 4, 2,
            64, 32, 32, 8,
            4, 2, 4, 8
    };

    auto keyBlock = new Block(roundKeyBytes);

    block->gamma()->tau()->theta()->sigma(keyBlock);

    block->sigma(keyBlock)->theta()->tau()->gamma();
    block->printAsMatrix();
}

void Test::err(string err) {
    cerr << err << endl;
}
