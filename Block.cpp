//
// Created by user on 20.11.17.
//

#include <iostream>
#include "Block.h"
#include "SBox.h"

static const byte tettaMaskData[] = {
        0x01, 0x02, 0x04, 0x06,
        0x02, 0x01, 0x06, 0x04,
        0x04, 0x06, 0x01, 0x02,
        0x06, 0x04, 0x02, 0x01
};

static const byte omegaMaskData[] = {
        0x01, 0x01, 0x01, 0x01,
        0x01, 0x02, 0x04, 0x08,
        0x01, 0x06, 0x24, 0xD8,
        0x01, 0x08, 0x40, 0x00
};

const byte* Block::tettaMask = tettaMaskData;
const byte* Block::omegaMask = omegaMaskData;


Block::Block() {
    bytes = new byte[16];
}

Block::Block(byte *b) {
    bytes = new byte[16];
    for (int i = 0; i < 16; i++) {
        set(i, b[i]);
    }
}

byte Block::get(int i) {
    return bytes[i];
}

byte Block::get(int i, int j) {
    return get(i * 4 + j);
}

void Block::set(int i, byte val) {
    bytes[i] = val;
}

byte Block::set(int i, int j, byte val) {
    set(i * 4 + j, val);
}

Block* Block::gamma() {
    for (int i = 0; i < 16; i++) {
        bytes[i] = SBox::bytes[bytes[i]];
    }
    return this;
}

Block* Block::tau() {
    byte tmp[16];
    for (int i = 0; i < 16; i++) {
        tmp[i] = get(i);
    }
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            set(i, j, tmp[j * 4 + i]);
        }
    }
    return this;
}

Block* Block::theta() {
    matrixProductMod(tettaMask);
    return this;
}

Block* Block::sigma(Block *roundKey) {
    for (int i = 0; i < 16; i++) {
        set(i, get(i) ^ roundKey->get(i));
    }
    return this;
}

Block* Block::pi() {
    byte tmp[16];
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            tmp[((i + j) % 4) * 4 + j] = get(i, j);
        }
    }
    for (int i = 0; i < 16; i++) {
        set(i, tmp[i]);
    }
    return this;
}

Block* Block::omega() {
    matrixProductMod(omegaMask);
    return this;
}

Block* Block::clone() {
    auto *b = new Block();
    for (int i = 0; i < 16; i++) {
        b->set(i, get(i));
    }
    return b;
}

void Block::matrixProductMod(const byte *b) {
    byte acc;
    byte tmp[16];
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            acc = 0;
            for (int k = 0; k < 4; k++) {
                acc ^= galoisMul(this->get(i, k), b[k * 4 + j]);
            }
            tmp[i * 4 + j] = acc;
        }
    }
    for (int i = 0; i < 16; i++) {
        set(i, tmp[i]);
    }
};

byte Block::galoisMul(byte a, byte b) {
    byte p = 0;
    byte counter;
    byte hi_bit_set;
    for (counter = 0; counter < 8; counter++) {
        if (b & 1)
            p ^= a;
        hi_bit_set = (a & 0x80);
        a <<= 1;
        if (hi_bit_set)
            a ^= 0x11d; /* x^8 + x^4 + x^3 + x^2 + 1 */
        b >>= 1;
    }
    return p;
}

string Block::toString() {
    auto str = *(new string);
    for (int i = 0; i < 16; i++) {
        str.push_back(bytes[i]);
    }
    return str;
}

void Block::printAsMatrix() {;
    for (int i = 0; i < 4; i++) {
        cout << "[ ";
        for (int j = 0; j < 4; j++) {
            cout << +get(i,j);
            cout << " ";
        }
        cout << "]\n";
    }
    cout << endl;
}
