//
// Created by user on 24.11.17.
//

#ifndef CRYPTO_2_FILEIOHELPER_H
#define CRYPTO_2_FILEIOHELPER_H


#include "Key.h"

class FileIOHelper {
public:
    void writeKey(string &filename, byte* key);
    Key* readKey(string &filename);

    byte* readFile(string &filename, int &length);
    void writeFile(string &filename, byte *text, int length);

    byte* readEncoded(string &filename, int &length);
    void writeEncoded(string &filename, byte *text, int length);
};


#endif //CRYPTO_2_FILEIOHELPER_H
