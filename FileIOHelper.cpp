//
// Created by user on 24.11.17.
//

#include "FileIOHelper.h"
#include <iostream>
#include <fstream>


void FileIOHelper::writeKey(string &filename, byte* key) {
    ofstream file;
    file.open (filename, ios::binary);
    for(int i = 0; i < 16; i++) {
        file << key[i];
    }
    file.close();
}

Key* FileIOHelper::readKey(string &filename) {
    ifstream file(filename, ios::binary);
    auto keyBytes = new byte[16];
    for(int i = 0; i < 16; i++) {
        file >> keyBytes[i];
    }
    file.close();
    return new Key(keyBytes);
}

byte *FileIOHelper::readFile(string &filename, int &length) {
    ifstream file(filename, ios::binary | std::ifstream::ate);
    if (!file.is_open()) {
        cout << "File read error." << endl;
    }
    length = (int)file.tellg();
    file.seekg(ios::beg);
    auto text = new byte[length];
    file.read((char*)text, length);
    file.close();
    return text;
}

void FileIOHelper::writeFile(string &filename, byte *text, int length) {
    ofstream file(filename, ios::binary);
    file.write((char*)text, length);
    file.close();
}

byte *FileIOHelper::readEncoded(string &filename, int &length) {
    int trueLength = length + 16 - (length % 16);
    ifstream file(filename, ios::binary);
    file.read((char*)&length, sizeof(int));
    auto text = new byte[trueLength];
    file.read((char*)text, trueLength);
    file.close();
    return text;
}

void FileIOHelper::writeEncoded(string &filename, byte *text, int length) {
    int trueLength = length + 16 - (length % 16);
    ofstream file(filename, ios::binary);
    file.write((char*)&length, sizeof(int));
    file.write((char*)text, trueLength);
    file.close();
}
