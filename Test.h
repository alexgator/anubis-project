//
// Created by user on 22.11.17.
//

#ifndef CRYPTO_2_TEST_H
#define CRYPTO_2_TEST_H

#include <iostream>
using namespace std;

class Test {
public:
    static void err(string err);
    static void sbox();
    static void invariants();
    static void invariantsSequence();
};


#endif //CRYPTO_2_TEST_H
