//
// Created by user on 24.11.17.
//

#ifndef CRYPTO_2_KEYGEN_H
#define CRYPTO_2_KEYGEN_H


#include "Block.h"

class Keygen {
public:
    Keygen();
    byte* generateKey();
};


#endif //CRYPTO_2_KEYGEN_H
