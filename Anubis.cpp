//
// Created by user on 20.11.17.
//

#include <random>
#include <iostream>
#include "Anubis.h"

using namespace std;

Anubis::Anubis() {
    this->key =  generateKey();
}

Anubis::Anubis(Key *key) {
    this->key = key;
}

Key* Anubis::generateKey() {
    auto buf = new byte[16];
    random_device rd;
    mt19937 gen(rd());
    uniform_int_distribution<> dis(0, 256);
    for (int i = 0; i < 16; i++){
        buf[i] = (byte)dis(gen);
    }
    auto key = new Key(buf);
    return key;
}

byte* Anubis::encode(const byte *message, int length) {
    auto blocks = new vector<Block*>();

    for (int i = 0; i < length; i += 16) {
        auto blockBytes = new byte[16] {0};
        for (int j = 0; j < 16 && i + j < length; j++) {
            blockBytes[j] = message[i + j];
        }
        auto block = new Block(blockBytes);
        blocks->push_back(block);
    }

    for (auto block : *blocks) {
        // Входное отбеливание - наложение нулевого подключа.
        block->sigma(key->roundKeys->at(0));
        for (int round = 1; round < key->rounds; round++) {
            block->gamma()->tau()->theta()->sigma(key->roundKeys->at((unsigned long)round));
        }
        // В последнем раунде не выполняется операция theta.
        block->gamma()->tau()->sigma(key->roundKeys->at((unsigned long)key->rounds));
    }

    int index = 0;
    auto result = new byte[blocks->size() * 16];
    for (auto block: *blocks) {
        for (int i = 0; i < 16; i++) {
            result[index++] = block->get(i);
        }
    }
    return result;
}

byte *Anubis::decode(const byte *message, int length) {
    auto blocks = new vector<Block*>();

    for (int i = 0; i < length; i += 16) {
        auto blockBytes = new byte[16];
        for (int j = 0; j < 16; j++) {
            blockBytes[j] = message[i + j];
        }
        auto block = new Block(blockBytes);
        blocks->push_back(block);
    }

    for (auto block : *blocks) {
        block->sigma(key->roundKeys->at((unsigned long)key->rounds))->tau()->gamma();
        for (int round = key->rounds - 1; round > 0; round--) {
            block->sigma(key->roundKeys->at((unsigned long)round))->theta()->tau()->gamma();
        }
        block->sigma(key->roundKeys->at(0));
    }

    int index = 0;
    auto result = new byte[length];
    for (auto block: *blocks) {
        for (int i = 0; i < 16; i++) {
            if (index >= length) {
                break;
            }
            result[index++] = block->get(i);
        }
    }
    return result;
}

