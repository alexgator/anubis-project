//
// Created by user on 20.11.17.
//

#include "Key.h"
#include "SBox.h"

Key::Key(byte *key) {
    initKey = new byte[16];
    for (int i = 0; i < 16; i++) {
        initKey[i] = key[i];
    }
    extend();
    calculateSubKeys();
}

void Key::extend() {
    ki->push_back(new Block(initKey));

    for (int i = 1; i <= rounds; i++) {
        auto tmp = ki->at(i - 1)->clone();
        f(tmp, i);
        ki->push_back(tmp);
    }
}

Block* Key::f(Block *block, int round) {
    block->gamma()->pi()->theta()->sigma(c(round));
}

Block* Key::c(int round) {
    auto tmp = new byte[16] {0};
    for (int i = 0; i < 4; i++) {
        tmp[i] = SBox::bytes[4 * (round - 1) + i];
    }
    return new Block(tmp);
}

void Key::calculateSubKeys() {
    for (int i = 0; i <= rounds; i++) {
        roundKeys->push_back(ki->at(i)->clone()->gamma()->omega()->tau());
    }
}
