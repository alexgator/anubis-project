//
// Created by user on 20.11.17.
//

#ifndef CRYPTO_2_BLOCK_H
#define CRYPTO_2_BLOCK_H


#include <string>

typedef unsigned char byte;
using namespace std;

class Block {
private:
    byte *bytes;

    void matrixProductMod(const byte *b);

    byte galoisMul(byte a, byte b);
public:
    static const byte* tettaMask;
    static const byte* omegaMask;

    Block();
    explicit Block(byte *b);
    byte get(int i);
    byte get(int i, int j);
    void set(int i, byte val);
    byte set(int i, int j, byte val);

    /**
     * Табличная замена каждого байта блока через SBox
     * */
    // Tested
    Block* gamma();

    /**
     * Байтовая перестановка. Массив представляется в виде матрицы 4x4,
     * строки преобразуются в столбцы a[i, j] = b[j, i]
     * */
    // Tested
    Block* tau();

    /**
     * Умножение массива на матрицу tettaMask
     * */
    // Tested
    Block* theta();

    /**
     * Наложение ключа раунда
     * */
    // Tested
    Block* sigma(Block *roundKey);

    /**
     * Циклический сдвиг столбцов таблицы вниз по правилу:
     * j-й столбец сдвигается на j позиций
     * */
    Block* pi();

    /**
     * Умножение массива на матрицу omegaMask
     * */
    Block* omega();

    Block* clone();

    string toString();

    void printAsMatrix();

};


#endif //CRYPTO_2_BLOCK_H
