#include <iostream>
#include "Anubis.h"
#include "Keygen.h"
#include "FileIOHelper.h"

typedef unsigned char byte;

void generateKey(string &keyFileName);
void encode(string &iFileName, string &oFileName, string &keyFileName);
void decode(string &iFileName, string &oFileName, string &keyFileName);
void help();

int main(int argc, char **argv) {

    auto arg = new vector<string*>();
    for (int i = 1; i < argc; i++) {
        arg->push_back(new string(argv[i ]));
    }

    if (*arg->at(0) == "-g") {
        generateKey(*arg->at(1));
    } else if (*arg->at(0) == "-e") {
        encode(*arg->at(2),*arg->at(3), *arg->at(1));
    } else if (*arg->at(0) == "-d") {
        decode(*arg->at(2),*arg->at(3), *arg->at(1));
    } else if (*arg->at(0) == "--help") {
        help();
    } else {
        printf("Call \"--help\" to show usage.");
    }
}

void help() {
    printf("\nUsage:\n");
    printf("\tgenerate key:\t-g [KeyFile]");
    printf("\tencode:\t-e [KeyFile] [InputFile] [outputFile]\n");
    printf("\tdecode:\t-d [KeyFile] [InputFile] [outputFile]");
}

void generateKey(string &keyFileName) {
    auto key = Keygen().generateKey();
    FileIOHelper().writeKey(keyFileName, key);
}

void encode(string &iFileName, string &oFileName, string &keyFileName) {
    auto io = new FileIOHelper();
    auto key = io->readKey(keyFileName);
    int length;
    auto text = io->readFile(iFileName, length);
    auto encoded = (new Anubis(key))->encode(text, length);
    io->writeEncoded(oFileName, encoded, length);
}

void decode(string &iFileName, string &oFileName, string &keyFileName) {
    auto io = new FileIOHelper();
    auto key = io->readKey(keyFileName);
    int length;
    auto encoded = io->readEncoded(iFileName, length);
    auto text = (new Anubis(key))->decode(encoded, length);
    io->writeFile(oFileName, text, length);
}
