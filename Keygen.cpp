//
// Created by user on 24.11.17.
//

#include <random>
#include "Keygen.h"

byte *Keygen::generateKey() {
    auto buf = new byte[16];
    random_device rd;
    mt19937 gen(rd());
    uniform_int_distribution<> dis(0, 256);
    for (int i = 0; i < 16; i++){
        buf[i] = (byte)dis(gen);
    }
    return buf;
}

Keygen::Keygen() = default;
