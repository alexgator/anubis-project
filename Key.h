//
// Created by user on 20.11.17.
//

#ifndef CRYPTO_2_KEY_H
#define CRYPTO_2_KEY_H


#include <vector>
#include "Block.h"

using namespace std;

class Key {
private:
    int size = 16;
    vector<Block*> *ki = new vector<Block*>();
    void extend();
    Block* f(Block *block, int round);
    Block* c(int round);
    void calculateSubKeys();
public:
    int rounds = size / 4 + 8;
    byte *initKey;
    explicit Key(byte* key);
    vector<Block*> *roundKeys = new vector<Block*>();
};


#endif //CRYPTO_2_KEY_H
