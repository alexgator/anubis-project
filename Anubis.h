//
// Created by user on 20.11.17.
//

#ifndef CRYPTO_2_ANUBIS_H
#define CRYPTO_2_ANUBIS_H

#include <string>
#include "Key.h"

using namespace std;

int main (int argc, char *argv[]);

class Anubis {
public:
    Key *key;
    explicit Anubis(Key *key);
    Anubis();

    byte* encode(const byte *message, int length);
    byte* decode(const byte *message, int length);

    Key* generateKey();

    void writeToFile(string *message, string *file);
    string readFromFile(string *file);
    void writeKeyToFile(Key *key, string *file);
    Key readKeyFromFile(string *file);
};


#endif //CRYPTO_2_ANUBIS_H
